$(document).ready(function () {
    $('.menu-burger, .menu-items').on('click', function() {
    $('.menu-bg, .menu-items, .menu-burger').toggleClass('fs');
    $('.menu-burger').text() == "☰" ? $('.menu-burger').text('✕') : $('.menu-burger').text('☰');
    });

    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });

    $("#jsValidateForm").validate();

    $("#jsValidateFormFooter").validate();

    $('#lightSlider').lightSlider({
        adaptiveHeight:false,
        pager: false,
        auto: true,
        speed: 500,
        pause: 5000,
        item:1,
        slideMargin:0,
        loop:true
    });
});